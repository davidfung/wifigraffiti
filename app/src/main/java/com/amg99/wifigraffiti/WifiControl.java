package com.amg99.wifigraffiti;

import android.content.Context;

public class WifiControl {

    // myst match the pref_theme_values defined in strings_activity_settings.xml.
    public static final String TERMINAL = "terminal";
    public static final String RAINBOW = "rainbow";

    private Context context;
    private String theme = WifiControl.TERMINAL;

    public WifiControl(Context context, String theme) {
        this.context = context;
        this.theme = theme;
    }

    public int getSignalColor(int level) {
        /**
         Returns corresponding color for the specified Wifi signal level in dbm.
         Default to the Rainbow theme.
         */
        int color;
        if (this.theme.equals(WifiControl.TERMINAL)) {
            color = this.getSignalColor_terminal(level);
        } else {
            color = this.getSignalColor_rainbow(level);
        }
        return color;
    }

    private int getSignalColor_terminal(int level) {
        /*
        This theme always return the color green.
        */
        int color = context.getResources().getColor(R.color.green);
        return color;
    }

    private int getSignalColor_rainbow(int level) {
        /*
        This theme is not linear and cluster towards -50 to -80 dbm.
        */
        int color;
        if      (level > (-50)) { color = context.getResources().getColor(R.color.signal_level_01); }
        else if (level > (-51)) { color = context.getResources().getColor(R.color.signal_level_02); }
        else if (level > (-52)) { color = context.getResources().getColor(R.color.signal_level_03); }
        else if (level > (-53)) { color = context.getResources().getColor(R.color.signal_level_04); }
        else if (level > (-54)) { color = context.getResources().getColor(R.color.signal_level_05); }
        else if (level > (-55)) { color = context.getResources().getColor(R.color.signal_level_06); }
        else if (level > (-56)) { color = context.getResources().getColor(R.color.signal_level_07); }
        else if (level > (-57)) { color = context.getResources().getColor(R.color.signal_level_08); }
        else if (level > (-58)) { color = context.getResources().getColor(R.color.signal_level_09); }
        else if (level > (-59)) { color = context.getResources().getColor(R.color.signal_level_10); }
        else if (level > (-60)) { color = context.getResources().getColor(R.color.signal_level_11); }
        else if (level > (-61)) { color = context.getResources().getColor(R.color.signal_level_12); }
        else if (level > (-62)) { color = context.getResources().getColor(R.color.signal_level_13); }
        else if (level > (-63)) { color = context.getResources().getColor(R.color.signal_level_14); }
        else if (level > (-64)) { color = context.getResources().getColor(R.color.signal_level_15); }
        else if (level > (-65)) { color = context.getResources().getColor(R.color.signal_level_16); }
        else if (level > (-66)) { color = context.getResources().getColor(R.color.signal_level_17); }
        else if (level > (-67)) { color = context.getResources().getColor(R.color.signal_level_18); }
        else if (level > (-68)) { color = context.getResources().getColor(R.color.signal_level_19); }
        else if (level > (-69)) { color = context.getResources().getColor(R.color.signal_level_20); }
        else if (level > (-70)) { color = context.getResources().getColor(R.color.signal_level_21); }
        else if (level > (-71)) { color = context.getResources().getColor(R.color.signal_level_22); }
        else if (level > (-72)) { color = context.getResources().getColor(R.color.signal_level_23); }
        else if (level > (-73)) { color = context.getResources().getColor(R.color.signal_level_24); }
        else if (level > (-74)) { color = context.getResources().getColor(R.color.signal_level_25); }
        else if (level > (-75)) { color = context.getResources().getColor(R.color.signal_level_26); }
        else if (level > (-76)) { color = context.getResources().getColor(R.color.signal_level_27); }
        else if (level > (-77)) { color = context.getResources().getColor(R.color.signal_level_28); }
        else if (level > (-78)) { color = context.getResources().getColor(R.color.signal_level_29); }
        else if (level > (-79)) { color = context.getResources().getColor(R.color.signal_level_30); }
        else if (level > (-80)) { color = context.getResources().getColor(R.color.signal_level_31); }
        else if (level > (-90)) { color = context.getResources().getColor(R.color.signal_level_32); }
        else if (level > (-120)) { color = context.getResources().getColor(R.color.signal_level_33); }
        else { color = context.getResources().getColor(R.color.no_signal); }
        return color;
    }
}
