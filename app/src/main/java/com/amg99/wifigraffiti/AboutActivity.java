package com.amg99.wifigraffiti;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WebView webview = new WebView(this);
        setContentView(webview);

        // Load about html content from assets.
        InputStream iS = null;
        try {
            iS = getAssets().open("about.html");
            BufferedReader reader = new BufferedReader(new InputStreamReader(iS));
            StringBuilder contents = new StringBuilder();
            char[] buffer = new char[4096];
            int read = 0;
            do {
                contents.append(buffer, 0, read);
                read = reader.read(buffer);
            } while (read >= 0);
            webview.loadData(contents.toString(), "text/html", "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
