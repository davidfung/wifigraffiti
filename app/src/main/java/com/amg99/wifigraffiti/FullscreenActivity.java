package com.amg99.wifigraffiti;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.amg99.wifigraffiti.util.SystemUiHider;

import java.util.List;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class FullscreenActivity extends Activity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
    private SystemUiHider mSystemUiHider;

    WifiManager wifiMgr;
    WifiReceiver wifiRcv;
    TextView tv;
    String lastTheme = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        // controlsView is not being used for now
        final View controlsView = findViewById(R.id.fullscreen_content_controls);

        tv = (TextView) findViewById(R.id.fullscreen_content);

        // set default theme
        SharedPreferences preferences = getDefaultSharedPreferences(getApplicationContext());
        String theme = preferences.getString("theme_list", "");
        if ( theme.equals("") ) {
            SharedPreferences.Editor prefEditor = preferences.edit();
            prefEditor.putString("theme_list", WifiControl.RAINBOW);
            prefEditor.commit();
        }


        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, tv, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        findViewById(R.id.settings_button).setOnTouchListener(mDelayHideTouchListener);

        // button handlers
        findViewById(R.id.about_button).setOnClickListener(buttonAboutHandler);
        findViewById(R.id.settings_button).setOnClickListener(buttonSettingsHandler);

        // Keep screen on in a polite manner
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Wifi Receiver
        wifiRcv = new WifiReceiver();
        registerReceiver(wifiRcv, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        // Wifi Manager
        wifiMgr = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiMgr.startScan();
    }

    protected void onPause() {
        // remember current theme for later comparison to detect change
        SharedPreferences preferences = getDefaultSharedPreferences(getApplicationContext());
        lastTheme = preferences.getString("theme_list", "");

        unregisterReceiver(wifiRcv);
        super.onPause();
    }

    protected void onResume() {
        // detect theme change
        SharedPreferences preferences = getDefaultSharedPreferences(getApplicationContext());
        String theme = preferences.getString("theme_list", "");
        if ( ! lastTheme.equals(theme) ) {
            /*
            Although the theme has changed, the app is still showing the previous theme until
            the next scan result comes back.  As a workaround, reset to scanning mode effectively
            disabling the previous theme.
            */
            displayScanningInProgress();
        }

        ensureWifiEnabled();
        registerReceiver(wifiRcv, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        if (wifiMgr != null) {
            wifiMgr.startScan();
        }
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        tv.setTextSize(getFontSize());
    }

    private void ensureWifiEnabled() {
        if (!wifiMgr.isWifiEnabled()) {
            Toast.makeText(getApplicationContext(), getText(R.string.enabling_wifi), Toast.LENGTH_LONG).show();
            wifiMgr.setWifiEnabled(true);
        }
    }

    private int getFontSize() {
        DisplayMetrics dMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dMetrics);

        // widthPixels is the absolute of the display in pixels
        // scaledDensity is a scaling factor for fonts displayed on the display
        // factor is an arbitrary factor, adjust it to get the desired font size
        final float widthPixels = dMetrics.widthPixels;
        final float scaledDensity = dMetrics.scaledDensity;
        final float factor = 10;
        final int fontSize;

        fontSize = (int) (widthPixels / scaledDensity / factor);
        return fontSize;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            mSystemUiHider.hide();
        }
    };

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    /**
     * Button Handlers
     */

    View.OnClickListener buttonAboutHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(getApplicationContext(), AboutActivity.class));
        }
    };

    View.OnClickListener buttonSettingsHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
        }
    };

    /**
     * Wifi Broadcast Receiver
     */
    class WifiReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (wifiMgr == null) {
                return;
            }

            // schedule a new scan
            wifiMgr.startScan();

            // find the strongest signal
            ScanResult sr, srMax;
            List<ScanResult> wifiList = wifiMgr.getScanResults();

            srMax = null;
            for(int i = 0; i < wifiList.size(); i++){
                sr = wifiList.get(i);
                if ((srMax == null) || (sr.level > srMax.level)) {
                    srMax = sr;
                }
            }

            // compute the signal color
            WifiControl wificontrol;
            SharedPreferences preferences = getDefaultSharedPreferences(getApplicationContext());
            String theme = preferences.getString("theme_list", "rainbow");
            wificontrol = new WifiControl(context, theme);

            if (srMax != null) {
                tv.setText(srMax.SSID);
                tv.setTextColor(wificontrol.getSignalColor(srMax.level));
            } else {
                displayScanningInProgress();
            }
        }
    }

    private void displayScanningInProgress() {
        tv.setText(R.string.scanning);
        tv.setTextColor(getApplicationContext().getResources().getColor(R.color.no_signal));
    }

}


